import React, { useRef, useEffect, useState } from 'react';
import StyledCheckBox from 'common/components/StyledCheckBox/';
import { loginUser } from 'common/api/actions';
import { useObjectState } from 'common/utils/hooks';
import useStore from 'common/store/';
import {
    StyledForm,
    StyledTitle,
    StyledInput,
    StyledFormLink,
    StyledConfirmButton,
    StyledButtonsRow,
    StyledOtherButton
} from 'common/components/formUtils/';
import InputField from 'common/components/formUtils/InputField'
import {
    SuccessMsg,
    ErrorMsg,
} from 'common/components/messages/';
import {
    validateEmailFormat,
    validateRequired,
    hasNoErrors
} from '../validators';
 
const LoginForm = (props) => {
    const [store, storeActions] = useStore();
 
    // Focus firstInput after LoginForm rendered
    const firstInput = useRef(null);
    useEffect(() => {
        firstInput.current.focus()
    }, [firstInput])
 
    const [success, setSuccess] = useState(null);
    const [values, setValues] = useObjectState({
        email: '',
        password: '',
        stayLoggedIn: false
    })
    const [errors, setErrors] = useObjectState({
        email: '',
        password: '',
    })
 
    const validate = () => {
        var _errors = {};
 
        if (!validateRequired(values.email))
            _errors.email = 'Nie podano emaila.'
        else if (!validateEmailFormat(values.email))
            _errors.email = 'Niepoprawny format email.'
        else
            _errors.email = ''
 
        if (!validateRequired(values.password))
            _errors.password = 'Nie podano hasła.'
        else
            _errors.password = ''
 
        setErrors({ ..._errors })
 
        return hasNoErrors(_errors);
    }
 
    // Make a request with form data
    const sendForm = () => {
        if (validate()) {
            loginUser({
                email: values.email,
                password: values.password,
                stay_logged_in: values.stayLoggedIn,
                onSuccess: (resp) => {
                    if (resp.status === 200) {
                        resp.json().then(data => {
                            setSuccess(true);
                            storeActions.user.logIn(data.username);
                        })
                    } else {
                        setSuccess(false);
                    }
                },
                onError: () => setSuccess(false)
            })
        }
    }
 
    // Call success action after success
    useEffect(() => {
        if(success)
            props.onSuccessAction();
    }, [success, props])
 
    const sendOnEnter = (e) => {
        if(e.key === 'Enter') {
            sendForm();
        }
    }
 
    if (success === null || success === false) {
        return (
            <>
                <StyledTitle>
                    ZALOGUJ SIĘ
                </StyledTitle>
                {success === false && (
                    <ErrorMsg marginBottom='0' center>
                        Błędny email lub hasło!
                    </ErrorMsg>
                )}
                <StyledForm autocomplete='off' noValidate>
                    <InputField label='Email' error={errors.email}>
                        <StyledInput
                            ref={firstInput}
                            type='text'
                            id='email'
                            value={values.email}
                            onChange={(e) => setValues({ email: e.target.value })}
                            onKeyDown={sendOnEnter}
                        />
                    </InputField>
 
                    <InputField label='Hasło' error={errors.password}>
                        <StyledInput
                            type='password'
                            id='password'
                            value={values.password}
                            onChange={(e) => setValues({ password: e.target.value })}
                            onKeyDown={sendOnEnter}
                        />
                    </InputField>
 
                    <InputField>
                        <StyledCheckBox
                            checked={values.stayLoggedIn}
                            onChange={() => setValues({ stayLoggedIn: !values.stayLoggedIn })}
                        >
                            Zostań zalogowany.
                        </StyledCheckBox>
                    </InputField>
 
                    <StyledFormLink onClick={props.clickPasswordResetLink}>
                        Nie pamiętam hasła.
                    </StyledFormLink>
                </StyledForm>
 
                <StyledButtonsRow>
                    <StyledConfirmButton onClick={() => sendForm()}>
                        Zaloguj się
                    </StyledConfirmButton>
                    <StyledOtherButton onClick={props.clickRegisterLink}>
                        Załóż konto
                    </StyledOtherButton>
                </StyledButtonsRow>
            </>
        )
    } else {
        return (
            <>
                <StyledTitle>
                    ZALOGOWANO
                </StyledTitle>
                <SuccessMsg center>
                    Zalogowano pomyślnie!
                </SuccessMsg>
            </>
        )
    }
}
 
export default LoginForm;